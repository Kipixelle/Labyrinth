import random

import harfang as hg
import math

from labyrinthe import CreateLaby, SaveLabydebugImg, SaveLaby
from labyrinthe3D import GetNextDir, GetKeyNode, CreateLabyModel, GetPlayerLightNode

#----------------------------- CONSTANTS ------------------------------------------------------------------------------
text_font = "../work/font/Stylish-Regular.ttf"

screen_width = 1900
screen_height = 1200
#----------------------------- GLOBALS --------------------------------------------------------------------------------

# Movement keys
MOVE_LEFT = hg.KeyQ
MOVE_RIGHT = hg.KeyD
MOVE_FRONT = hg.KeyZ
MOVE_BACK = hg.KeyS
ACTION = hg.KeyE

movements = {} # Directions corresponding to the movements of the player

# Score and timer
player_score = []
enreg_score = {"time" : [], "succ_laby" : []}
timer = 0

# Sound and channel
soundWalking = None
soundStakeTrap = None
soundArrowTrap = None
soundStoneTrap = None
soundKey = None
channelWalking = None
soundPotion = None
soundBreathing = None

# Labyrinth construction
main_road_len = 50
max_second_road = 8
min_second_road = 5
second_road_max_len = 15
max_trap = 2
min_trap = 1

# Game params
trap_weight_max = 4
door_opened = False
orthoView = False
nbr_loop = 0
player_health = 100

class Player:

	def __init__(self, coord, dir, player_node):
		self.node = player_node
		self.coord_in_laby = coord # ex: [x, y]
		self.direction = dir
		self.position = hg.Vector3(0, 1, 0)
		self.inventory = {}
		#caracteristics
		self.health = player_health
		self.dexterity = 0
		self.intelligence = 0
		self.rapidity = 0

	def hasKey(self): return "key" in self.inventory.keys()

	def addKeyInInventory(self): self.inventory["key"] = 1

	def getPotionNbr(self): return self.inventory["potion"]

	def setPotionNbr(self, potion_nbr): self.inventory["potion"] = potion_nbr

	def usePotion(self):
		if self.inventory["potion"] > 0:
			self.inventory["potion"] -= 1
			self.health += int((100 - self.health) * 0.5)

	def getDirection(self): return self.direction

	def getDexterity(self): return self.dexterity

	def getIntelligence(self): return self.intelligence

	def getRapidity(self): return self.rapidity

	def getHealth(self): return self.health

	def getPosition(self): return self.position

	def getDirection(self): return self.direction

	def getLabyCoord(self): return self.coord_in_laby

	def setPosition(self, new_position): self.position = new_position

	def setLabyCoord(self, new_laby_coord): self.coord_in_laby = new_laby_coord

	def setDirection(self, new_direction): self.direction = new_direction

	def setHealth(self, new_health): self.health = new_health

	def setChacract(self, rapidity, dexterity, intelligence):
		self.rapidity = rapidity
		self.dexterity = dexterity
		self.intelligence = intelligence


class Message:
	def __init__(self, x, y, text, size, color, font, delay):
		self.x = x
		self.y = y
		self.text = text
		self.size = size
		self.color = color
		self.delay = delay
		self.font = font

#-------------------------------------------------------TOOLS-----------------------------------------------------------
def GetNeighRoadCoord(coord, roads): # Give the neiboring existing cases of an other case
	neigh = []
	for road_idx in range(0, len(roads)):
		for idx, case in enumerate(roads[road_idx]):
			case_coord = case.coord
			if [coord[0] + 1, coord[1]] == case_coord or [coord[0] - 1, coord[1]] == case_coord or \
							[coord[0], coord[1] + 1] == case_coord or [coord[0], coord[1] - 1] == case_coord:
				neigh.append(case_coord)
	return neigh


def GetCase(laby_roads, coord): # Get the case object in the labyrinth
	for road in laby_roads:
		for case in road:
			if case.coord == coord:
				return case
	return None


def GetAngleByDir(direction): # Give the angle corresponding to the player direction
	if direction == [0, 1]:
		return 0
	elif direction == [0, -1]:
		return 180
	elif direction == [1, 0]:
		return 90
	else: # direction == [-1, 0]
		return -90


def SetMessage(x, y, text, size, color, font, delay): # Define a new message
	global message
	message = Message(x, y, text, size, color, font, delay)


def GetTimer():
	global timer
	return round(timer, 2)


def GetNLoop():
	global nbr_loop
	return nbr_loop


def GetChannelWalking():
	global channelWalking
	return channelWalking


def PlayerHasWin(player, plus): # Check if the player has won
	win = player.hasKey() and door_opened
	if win:
		SetMessage(screen_width / 2 - 100, screen_height - 100, "YOU WIN!", 50, hg.Color.White,
				   text_font, 2)
	return win


def PlayerIsDie(player, message, plus): # Check if the player has lost the game
	die = player.getHealth() <= 0
	if die and message.delay <= 0:
		SetMessage(screen_width / 2 - 100, screen_height - 100, "YOU DIE!", 50, hg.Color.White,
				   text_font, 3)
	return die
#-----------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------INITIALISATION-------------------------------------------------------
def InitPlayer(plus, scn): # Initialize player's parameters

	player_path = "assets/laby/player.geo"
	player_dir = movements["up_dir"]

	# ADD PHYSIC PLAYER
	player_matrix = hg.Matrix4.TransformationMatrix(hg.Vector3(0, 1, 0), hg.Vector3(0, math.radians(GetAngleByDir(player_dir)), 0))
	player_node = plus.AddGeometry(scn, player_path, player_matrix)

	# ADD VIRTUAL PLAYER
	player = Player([0, 0], player_dir, player_node)

	return player


def InitDebug(plus):

	laby1 = CreateLaby(main_road_len, random.randint(min_second_road, max_second_road), second_road_max_len, max_trap,
					   min_trap)
	InitMovements(laby1.roads[0])
	scene, cam, fps = InitScene(plus)
	CreateLabyModel(laby1.roads, scene, plus)

	return laby1, scene, cam, fps


def InitMovements(main_road): # Initialize player's directions depending on the labyrinth's orientation
	global movements
	movements["up_dir"] = GetNextDir(main_road[0].coord, main_road[1].coord)
	movements["back_dir"] = [-movements["up_dir"][0], -movements["up_dir"][1]]
	DefineLeftRightDir()


def DefineLeftRightDir(): # Define the right and left direction depending on the up and down ones
	if movements["up_dir"] == [0, 1] and movements["back_dir"] == [0, -1]:
		movements["left_dir"] = [-1, 0]
		movements["right_dir"] = [1, 0]
	elif movements["up_dir"] == [0, -1] and movements["back_dir"] == [0, 1]:
		movements["left_dir"] = [1, 0]
		movements["right_dir"] = [-1, 0]
	elif movements["up_dir"] == [1, 0] and movements["back_dir"] == [-1, 0]:
		movements["left_dir"] = [0, 1]
		movements["right_dir"] = [0, -1]
	else:# movements["up_dir"] == [-1, 0] and movements["down_dir"] == [1, 0]:
		movements["left_dir"] = [0, -1]
		movements["right_dir"] = [0, 1]


def InitScene(plus): # Initialize the 3D scene
	global orthoView
	scn = plus.NewScene()

	while not scn.IsReady():
		plus.UpdateScene(scn, plus.UpdateClock())

	cam_rot_y = GetAngleByDir(movements["up_dir"])
	cam = plus.AddCamera(scn, hg.Matrix4.TransformationMatrix(hg.Vector3(0, 4, 0),
															  hg.Vector3(math.radians(90), math.radians(cam_rot_y),
																		 0)))
	cam.GetCamera().SetZoomFactor(1.8)

	cam.GetCamera().SetOrthographicSize(8)

	if orthoView:
		ChangeCameraView(cam)

	fps = hg.FPSController(0, 8, -5)
	fps.SetRot(hg.Vector3(45, 0, 0))

	return scn, cam, fps


def InitGame(plus, dexterity, intelligence, rapidity): # Initialize the game
	global timer, door_opened
	timer = 0
	door_opened = False

	#----- LABY FILE ------------------------
	laby1 = CreateLaby(main_road_len, random.randint(min_second_road, max_second_road), second_road_max_len, max_trap, min_trap)
	SaveLabydebugImg(laby1, "laby_debug_img.png")
	SaveLaby(laby1, "laby_save.txt")
	#----- GAME -----------------------------
	InitMovements(laby1.roads[0])
	#----- SCENE ----------------------------
	scene, cam, fps = InitScene(plus)
	#----- PLAYER ---------------------------
	player = InitPlayer(plus, scene)
	player.setChacract(dexterity, intelligence, rapidity)
	#----- TEXT -----------------------------
	SetMessage(plus.GetScreenWidth()/2 - 100, plus.GetScreenHeight()/2, "message non configuré", 30, hg.Color.White, text_font, 0)
	#----- LABY 3D --------------------------
	CreateLabyModel(laby1.roads, scene, plus)

	UpdateCamera(cam, player)

	return laby1, scene, cam, fps, player


def InitSound(plus):
	global soundWalking, soundStakeTrap, soundArrowTrap, channel_walking, soundKey, soundStoneTrap, soundPotion, soundBreathing
	mixer = plus.GetMixer()
	soundWalking = mixer.LoadSound("../work/audio/walking.ogg")
	soundStakeTrap = mixer.LoadSound("../work/audio/stake_trap.ogg")
	soundArrowTrap = mixer.LoadSound("../work/audio/arrow_trap.ogg")
	soundStoneTrap = mixer.LoadSound("../work/audio/stone_trap.ogg")
	soundKey = mixer.LoadSound("../work/audio/key.ogg")
	channel_walking = None
	soundPotion = mixer.LoadSound("../work/audio/potion.ogg")
	soundBreathing = mixer.LoadSound("../work/audio/respiration.ogg")


def ReloadGame():
	global main_road_len, min_second_road, max_second_road, second_road_max_len, max_trap, trap_weight_max, player_health, nbr_loop, player_score, enreg_score

	#----- VARIABLE LABY ------------------------
	main_road_len = 50
	max_second_road = 8
	min_second_road = 5
	second_road_max_len = 15
	max_trap = 2
	trap_weight_max = 4
	player_health = 100
	nbr_loop = 0
	player_score = []
	enreg_score = {"time": [], "succ_laby": []}

#----------------------------------------------- MAJ -------------------------------------------------------------------
def UpdatePlayer(player, laby_roads, plus):  # Update the player
	if not player.getHealth() <= 0:
		Action(player, laby_roads, plus)
		MovePlayer(player, laby_roads, plus)

		player_light_node = GetPlayerLightNode()
		player_pos = player.getPosition()
		player_light_node.GetTransform().SetPosition(hg.Vector3(player_pos.x, 2, player_pos.z))


def UpdateMessage(old_message):  # Update the message
	global message
	if old_message is None or old_message.text != message.text:
		return message
	return old_message


def UpdateCamera(cam_node, player):  # Update the camera
	player_pos = player.getPosition()

	if orthoView:
		cam_pos = hg.Vector3(player_pos.x - movements["up_dir"][0] * 2 - movements["right_dir"][0] * 2, 4,
							 player_pos.z - movements["up_dir"][1] * 2 - movements["right_dir"][1] * 2)
	else:
		cam_pos = hg.Vector3(player_pos.x, 4, player_pos.z)

	cam_node.GetTransform().SetPosition(cam_pos)


def UpdateGame():  # Update the game parameters and increase the difficulty when player win a level
	global nbr_loop, main_road_len, max_second_road, min_second_road, second_road_max_len, max_trap, min_trap, trap_weight_max, player_health
	nbr_loop += 1
	if nbr_loop % 2 == 0:
		main_road_len += 5
		max_second_road += 2
		min_second_road += 2
		second_road_max_len += 2
	if nbr_loop % 5 == 0:
		max_trap += 1
		min_trap += 1
		trap_weight_max += 1
		player_health += 15


def UpdateScore():  # Update the score
	global player_score, timer
	player_score.append(timer)
	timer = 0


def UpdateTimer(time):  # Update the timer
	global timer
	timer += time


def ChangeCameraView(cam):  # Change the camera view (orthonormal or top view)
	global orthoView, MOVE_RIGHT, MOVE_LEFT, MOVE_FRONT, MOVE_BACK

	ortho = cam.GetCamera().GetOrthographic()
	cam.GetCamera().SetOrthographic(not ortho)

	cam_rot_y = GetAngleByDir(movements["up_dir"])
	if not ortho:
		cam.GetTransform().SetRotation(hg.Vector3(math.radians(50), math.radians(cam_rot_y + 50), 0))

		MOVE_FRONT = hg.KeyQ
		MOVE_RIGHT = hg.KeyZ
		MOVE_BACK = hg.KeyD
		MOVE_LEFT = hg.KeyS

		orthoView = True

	else:
		cam.GetTransform().SetRotation(hg.Vector3(math.radians(90), math.radians(cam_rot_y), 0))

		MOVE_FRONT = hg.KeyZ
		MOVE_RIGHT = hg.KeyD
		MOVE_BACK = hg.KeyS
		MOVE_LEFT = hg.KeyQ

		orthoView = False


#----------------------------------------------- SCORE -----------------------------------------------------------------
def EnregScore():  # Register the score after a level
	global player_score

	enreg_time = []
	enreg_successive_laby = []

	# If the file is empty or doesn't exist
	try:
		with open("score.txt", "r", encoding='utf-8') as file:
			nbr_line = len(file.readlines())
	except:
		with open("score.txt", "w", encoding='utf-8') as file:
			pass
		with open("score.txt", "r", encoding='utf-8') as file:
			nbr_line = len(file.readlines())

	if nbr_line == 0:
		with open("score.txt", "w", encoding='utf-8') as file:
			file.write("0\n0")

	# If the file is full and the score not empty
	with open("score.txt", "r", encoding='utf-8') as file:
		nbr_time = int(file.readline())
		for x in range(nbr_time):
			time = file.readline()
			enreg_time.append(round(float(time), 2))
		nbr_laby = int(file.readline())
		for x in range(nbr_laby):
			nbr_laby = file.readline()
			enreg_successive_laby.append(int(nbr_laby))

	if len(enreg_time) == 0:
		player_score.sort()
		for x in range(min(5, len(player_score))):
			enreg_time.append(player_score[x])
	else:
		if len(enreg_time) == 5:
			for sc in player_score:
				found = False
				for idx, esc in enumerate(enreg_time):
					if sc < esc and not found:
						enreg_time.remove(enreg_time[idx])
						enreg_time.insert(idx, sc)
						found = True
		else:
			for sc in player_score:
				enreg_time.append(sc)
			enreg_time.sort()

			to_delete = []

			for x in range(5, len(enreg_time)):
				to_delete.append((enreg_time[x]))

			for elem in to_delete:
				enreg_time.remove(elem)

	if len(enreg_successive_laby) == 0:
		enreg_successive_laby.append(len(player_score))
	else:
		if enreg_successive_laby == 5:
			found = False
			for idx, esc in enumerate(enreg_successive_laby):
				if len(player_score) > esc and not found:
					enreg_successive_laby.remove(enreg_successive_laby[idx])
					enreg_successive_laby.insert(idx, len(player_score))
					found = True
		else:
			enreg_successive_laby.append(len(player_score))
			enreg_successive_laby.sort(reverse=True)

			to_delete = []

			for x in range(5, len(enreg_successive_laby)):
				to_delete.append((enreg_successive_laby[x]))

			for elem in to_delete:
				enreg_successive_laby.remove(elem)

	with open("score.txt", "w", encoding='utf-8') as file:
		file.write(str(len(enreg_time)) + "\n")
		for x in range(len(enreg_time)):
			file.write(str(enreg_time[x]) + "\n")
		file.write(str(len(enreg_successive_laby)) + "\n")
		for y in range(len(enreg_successive_laby)):
			file.write(str(enreg_successive_laby[y]) + "\n")


def LoadScore():  # Get the best score in the file
	global enreg_score
	enreg_score["time"] = []
	enreg_score["succ_laby"] = []
	try:
		with open("score.txt", "r", encoding='utf-8') as file:
			nbr_time = int(file.readline())
			for x in range(nbr_time):
				time = file.readline()
				enreg_score["time"].append(round(float(time.rstrip('\n')), 2))
			nbr_laby = int(file.readline())
			for x in range(nbr_laby):
				nbr_laby = file.readline()
				enreg_score["succ_laby"].append(int(nbr_laby.rstrip('\n')))

		len_enreg_score_time = len(enreg_score["time"])
		if len_enreg_score_time < 5:
			for x in range(5 - len_enreg_score_time):
				enreg_score["time"].append('')

		len_enreg_score_succ_laby = len(enreg_score["succ_laby"])
		if len_enreg_score_succ_laby < 5:
			for x in range(5 - len_enreg_score_succ_laby):
				enreg_score["succ_laby"].append('')

	except:
		enreg_score["time"] = ['', '', '', '', '']
		enreg_score["succ_laby"] = ['', '', '', '', '']

	return enreg_score

#---------------------------------------------- PLAYER ACTION ----------------------------------------------------------
def Action(player, laby_roads, plus): # Check if the player make a special action (get the key, went from the labyrinth, walking into a trap)
	global key_found, door_opened, view_1_p

	case = GetCase(laby_roads, player.getLabyCoord())

	#----TRAP
	if case.hasTrap() and case.trapActivated():
		trap = case.getTrap()

		# Define the damage cause by the trap
		trap_weight = random.randint(1, 4)
		player_weight = random.randint(0, 3)

		trap_rapidity = trap.getRapidity() * trap_weight
		trap_dexterity = trap.getDexterity() * trap_weight
		trap_intelligence = trap.getIntelligence() * trap_weight

		player_rapidity = player.getRapidity() * player_weight
		player_dexterity = player.getDexterity() * player_weight
		player_intelligence = player.getIntelligence() * player_weight

		damage = min(max(player_rapidity - trap_rapidity, -trap_rapidity), 0) + \
				 min(max(player_dexterity - trap_dexterity, -trap_dexterity), 0) + \
				 min(max(player_intelligence - trap_intelligence, -trap_intelligence), 0)

		player.setHealth(min(max(player.getHealth() + damage, 0), 100))
		trap.desactivate()

		# Setup information message
		if trap.getType() == "Arrow":
			plus.GetMixer().Start(soundArrowTrap)
			msg = "Arrows hurt you !"
		elif trap.getType() == "Stake":
			plus.GetMixer().Start(soundStakeTrap)
			msg = "Pikes cut you !"
		else: # trap.getType() == "Ball":
			plus.GetMixer().Start(soundStoneTrap)
			msg = "A rock fall !"

		SetMessage(screen_width / 2 - 130, screen_height - 100, msg  + "   " + str(damage) + "HP", 28, hg.Color.Red, text_font, 3)

	#----KEY "E" ACTION
	if plus.KeyPress(ACTION):
		# Player get the key
		if case.hasKey():
			case.object.remove("key")
			player.addKeyInInventory()
			SetMessage(screen_width / 2 - 94, screen_height - 100, "You find the key!", 28, hg.Color.White, text_font, 3)
			key_node = GetKeyNode()
			key_node.SetEnabled(False)

			plus.GetMixer().Start(soundKey)

		# Player find the exit
		if case.isExit() and player.hasKey():
			door_opened = True

	# Player drink a health potion
	if plus.KeyPress(hg.KeyR):
		plus.GetMixer().Start(soundPotion)
		player.usePotion()


def FloatIsEqual(a, b, e = 0.0001):  # Compare two float
	return abs(b - a) < e

#---------------------------------------------------- PLAYER MOUVEMENTS ------------------------------------------------
dir_label = None

def MovePlayer(player, laby_roads, plus): # Calculate and update the player's postion
	global dir_label, channelWalking

	# Player information
	pos = player.node.GetTransform().GetPosition()
	player_laby_coord = player.getLabyCoord()
	player_direction = player.getDirection()

	# If player in the center of a case
	if FloatIsEqual(player_laby_coord[0] * 2, pos.x) and FloatIsEqual(player_laby_coord[1] * 2, pos.z):
		neigh_case = GetNeighRoadCoord(player_laby_coord, laby_roads)

		# Test keys
		if plus.KeyDown(MOVE_FRONT):
			dir_label = "up_dir"

		elif plus.KeyDown(MOVE_BACK):
			dir_label = "back_dir"

		elif plus.KeyDown(MOVE_LEFT):
			dir_label = "left_dir"

		elif plus.KeyDown(MOVE_RIGHT):
			dir_label = "right_dir"

		else:
			dir_label = None

		# if a key (on the keyboard) is activate
		if dir_label != None:

			in_same_dir = CheckAndChangePlayerDir(dir_label, player)  # Update direction
			player_dir = player.getDirection()  # Get the new direction

			if in_same_dir:  # If the direction didn't change
				next_case = [player_laby_coord[0] + player_dir[0], player_laby_coord[1] + player_dir[1]]
				if next_case in neigh_case:  # And it exist a case in this direction
					# Move of one case
					player.setLabyCoord(next_case)
					player_laby_coord = next_case
					# Sound
					if channelWalking is None:
						channelWalking = plus.GetMixer().Start(soundWalking, hg.Mixer.RepeatState)
					n = random.randint(0, 100)
					if n == 50:
						plus.GetMixer().Start(soundBreathing)

				else:
					dir_label = None

			else: # If the direction change
				# Change the player's direction
				new_rot = hg.Vector3(0, math.radians(GetAngleByDir(player_dir)), 0)
				player.node.GetTransform().SetRotation(new_rot)

		else:
			if channelWalking is not None:
				plus.GetMixer().Stop(channelWalking)
				channelWalking = None

	# If player in movement, create an interpolation between the origin's case and the next case
	if dir_label is not None:
		if player_laby_coord[0] * 2 > pos.x:
			x = min(max(pos.x + 0.05 * player_direction[0] * 2, pos.x), player_laby_coord[0] * 2)
		else:
			x = max(min(pos.x + 0.05 * player_direction[0] * 2, pos.x), player_laby_coord[0] * 2)

		if player_laby_coord[1] * 2 > pos.z:
			z = min(max(pos.z + 0.05 * player_direction[1] * 2, pos.z), player_laby_coord[1] * 2)
		else:
			z = max(min(pos.z + 0.05 * player_direction[1] * 2, pos.z), player_laby_coord[1] * 2)


		player.setPosition(hg.Vector3(x, 1, z))
		player.node.GetTransform().SetPosition(player.position)


def CheckAndChangePlayerDir(dir_label, player): # Check if the player is in the same direction
	player_dir = player.getDirection()
	if player_dir == movements[dir_label]:
		return True
	else:
		player.setDirection(movements[dir_label])
		return False