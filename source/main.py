import harfang as hg

from game import InitGame, UpdatePlayer, UpdateCamera, UpdateMessage, UpdateScore, UpdateTimer, UpdateGame, \
	EnregScore, LoadScore, PlayerHasWin, PlayerIsDie, GetTimer, ReloadGame, GetNLoop, InitSound, \
	GetChannelWalking, ChangeCameraView

hg.SetLogIsDetailed(True)
hg.SetLogLevel(hg.LogAll)

hg.LoadPlugins()

screenHeight = 1200
screenWidth = 1900

plus = hg.GetPlus()
plus.RenderInit(screenWidth, screenHeight)
plus.Mount("./")
plus.AudioInit()

#----------------------------- CONSTANTS ------------------------------------------------------------------------------
title_font = "../work/font/MontserratSubrayada-Bold.ttf"
press_to_continue_font = "../work/font/Pangolin-Regular.ttf"
text_font = "../work/font/Stylish-Regular.ttf"

textureTitleBack = plus.GetRendererAsync().LoadTexture("../work/picture/laby.PNG")
texturePaper = plus.GetRendererAsync().LoadTexture("../work/picture/parchemin.jpeg")
textureKey = plus.GetRendererAsync().LoadTexture("../work/picture/key.png")
textureKey_2 = plus.GetRendererAsync().LoadTexture("../work/picture/key_2.png")
texturePotion = plus.GetRendererAsync().LoadTexture("../work/picture/health_potion.png")
texturePotion_2 = plus.GetRendererAsync().LoadTexture("../work/picture/health_potion_2.png")
textureHeart = plus.GetRendererAsync().LoadTexture("../work/picture/heart.png")

mixer = plus.GetMixer()
soundAmbiance = mixer.LoadSound("../work/audio/principal_theme.ogg")
soundBreathing = mixer.LoadSound("../work/audio/respiration.ogg")
soundChoice = mixer.LoadSound("../work/audio/choice.ogg")
soundCloseDoor = mixer.LoadSound("../work/audio/close_door.ogg")
soundOpenDoor = mixer.LoadSound("../work/audio/open_door.ogg")

#----------------------------- GLOBALS --------------------------------------------------------------------------------
points = 10
dexterity = 0
rapidity = 0
intelligence = 0
charac_to_change = 0
dexterity_color = hg.Color.White
rapidity_color = hg.Color.White
intelligence_color = hg.Color.White

pause_option = 0
restart_option = 0
yes_color = hg.Color.White
no_color = hg.Color.White

scene = None
cam = None
fps = None
player = None
laby1 = None
message = None

channel_ambiante = None
channel_open_door = None
channel_close_door = None

cpt_win = 0

fps_cam = False


def GetScreenHeight():
	global screenHeight
	return screenHeight

def GetScreenWidth():
	global screenWidth
	return screenWidth

#---------------------------------------------- STATES -----------------------------------------------------------------
def Title():
	global game_state, fade_value

	# DISPLAY
	plus.Quad2D(0, 0, 0, screenHeight, screenWidth, screenHeight, screenWidth, 0, hg.Color.White, hg.Color.White,
				hg.Color.White, hg.Color.White, textureTitleBack)
	plus.Text2D(screenWidth / 2 - 375, screenHeight / 2 + 250, 'Labyrinth 3D', 100, hg.Color.White, title_font)
	plus.Text2D(screenWidth / 2 - 500, screenHeight / 2 - 400, "H : Help and Rules", 30, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 + 380, screenHeight / 2 - 400, "S : Scores", 30, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 300, screenHeight / 2 - 300, 'PRESS "SPACE" TO CONTINUE', 50, hg.Color.White, text_font)

	# FADE
	if fade_value > 0:
		fade_value -= fade_coeff
		color = hg.Color(0, 0, 0, fade_value)
		plus.Quad2D(0, 0, 0, screenHeight, screenWidth, screenHeight, screenWidth, 0, color, color, color, color)

	# CHANGE STATE
	if plus.KeyPress(hg.KeySpace):
		game_state = DefinePlayerCharact
	if plus.KeyPress(hg.KeyH):
		game_state = Help
	if plus.KeyPress(hg.KeyS):
		game_state = Score


def Help():
	global game_state

	# DISPLAY
	color = hg.Color(1, 1, 1, 0.5)
	plus.Quad2D(0, 0, 0, screenHeight, screenWidth, screenHeight, screenWidth, 0, color, color,
				color, color, textureTitleBack)

	plus.Text2D(screenWidth / 2 - 107, screenHeight / 2 + 350, 'HELP', 100, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 450, screenHeight / 2 + 200, "Movements : ", 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 360, screenHeight / 2 + 120, "Z", 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 403, screenHeight / 2 + 80, "Q S D", 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 + 215, screenHeight / 2 + 210, "Interact : 'E'", 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 + 215, screenHeight / 2 + 110, "Pause : 'P'", 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 450, screenHeight / 2 - 10, "Using Potion : 'R' give you 50% of your lost life", 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 450, screenHeight / 2 - 110, "Orthogonal view : 'C' (easier)", 50, hg.Color.White,
				text_font)
	plus.Text2D(screenWidth - 200, screenHeight / 2 - 200, "Rule >", 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 422, screenHeight / 2 - 300, 'PRESS "SPACE" TO RETURN TO THE TITLE', 50, hg.Color.White, text_font)

	# CHANGE STATE
	if plus.KeyPress(hg.KeySpace):
		game_state = Title
	if plus.KeyPress(hg.KeyD) or plus.KeyPress(hg.KeyRight):
		game_state = Rule


def Rule():
	global game_state

	# DISPLAY
	color = hg.Color(1, 1, 1, 0.5)
	plus.Quad2D(0, 0, 0, screenHeight, screenWidth, screenHeight, screenWidth, 0, color, color,
				color, color, textureTitleBack)

	plus.Text2D(screenWidth / 2 - 133, screenHeight / 2 + 350, 'RULES', 100, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 554, screenHeight / 2 + 200, "You have find the key in the labyrinth, and go to the exit.", 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 470, screenHeight / 2 + 50, "Watch out ! there are some traps on your road...", 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 445, screenHeight / 2 - 100, "Labyrinth follows the other, until your death.", 50, hg.Color.White, text_font)

	plus.Text2D(80, screenHeight / 2 - 200, "< Help", 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 422, screenHeight / 2 - 300, 'PRESS "SPACE" TO RETURN TO THE TITLE', 50, hg.Color.White, text_font)

	# CHANGE STATE
	if plus.KeyPress(hg.KeySpace):
		game_state = Title
	if plus.KeyPress(hg.KeyQ) or plus.KeyPress(hg.KeyLeft):
		game_state = Help


def Score():
	global game_state

	# DISPLAY
	score = LoadScore()

	color = hg.Color(1, 1, 1, 0.5)
	plus.Quad2D(0, 0, 0, screenHeight, screenWidth, screenHeight, screenWidth, 0, color, color,
				color, color, textureTitleBack)

	plus.Text2D(screenWidth / 2 - 276, screenHeight / 2 + 350, 'BEST SCORES', 100, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 600, screenHeight / 2 + 250, "-- Time --", 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 600, screenHeight / 2 + 170, "1.    " + str(score["time"][0]), 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 600, screenHeight / 2 + 80, "2.    " + str(score["time"][1]), 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 600, screenHeight / 2 - 10, "3.    " + str(score["time"][2]), 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 600, screenHeight / 2 - 100 , "4.    " + str(score["time"][3]), 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 600, screenHeight / 2 - 190, "5.    " + str(score["time"][4]), 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 + 100, screenHeight / 2 + 250, "-- Successive labyrinth --", 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 + 250, screenHeight / 2 + 170, "1.    " + str(score["succ_laby"][0]), 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 + 250, screenHeight / 2 + 80, "2.    " + str(score["succ_laby"][1]), 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 + 250, screenHeight / 2 - 10, "3.    " + str(score["succ_laby"][2]), 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 + 250, screenHeight / 2 - 100 , "4.    " + str(score["succ_laby"][3]), 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 + 250, screenHeight / 2 - 190, "5.    " + str(score["succ_laby"][4]), 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 422, screenHeight / 2 - 300, 'PRESS "SPACE" TO RETURN TO THE TITLE', 50, hg.Color.White, text_font)

	# CHANGE STATE
	if plus.KeyPress(hg.KeySpace):
		game_state = Title


def DefinePlayerCharact():
	global points, dexterity, intelligence, rapidity
	global dexterity_color, intelligence_color, rapidity_color
	global laby1, scene, cam, fps, player
	global game_state, charac_to_change, fade_value, channel_ambiante

	# CHARACT TO CHANGE COLOR
	if charac_to_change == 0:
		dexterity_color = hg.Color(4/255, 124/255, 24/255)
		intelligence_color = hg.Color.White
		rapidity_color = hg.Color.White
	elif charac_to_change ==1:
		dexterity_color = hg.Color.White
		intelligence_color = hg.Color(4/255, 124/255, 24/255)
		rapidity_color = hg.Color.White
	elif charac_to_change == 2:
		dexterity_color = hg.Color.White
		intelligence_color = hg.Color.White
		rapidity_color = hg.Color(4/255, 124/255, 24/255)

	# DISPLAY
	plus.Quad2D(0, 0, 0, screenHeight, screenWidth, screenHeight, screenWidth, 0, hg.Color.White, hg.Color.White,
				hg.Color.White, hg.Color.White, textureTitleBack)
	plus.Text2D(100, screenHeight / 2 + 250, "Choose your characteristics ", 50, hg.Color.White, text_font)
	plus.Text2D(150, screenHeight / 2 + 150,  str(points) + " points to place", 35, hg.Color.White, text_font)
	plus.Text2D(100, screenHeight / 2 + 50, "Dexterity : " + str(dexterity), 35, dexterity_color, text_font)
	plus.Text2D(100, screenHeight / 2 - 50, "Intelligence : " + str(intelligence), 35, intelligence_color, text_font)
	plus.Text2D(100, screenHeight / 2 - 150, "Rapidity : " + str(rapidity), 35, rapidity_color, text_font)
	plus.Text2D(screenWidth / 2 - 300, screenHeight / 2 - 300, 'PRESS "SPACE" TO CONTINUE', 50, hg.Color.White, text_font)

	# NAVIGATION
	if plus.KeyPress(hg.KeyS):
		charac_to_change += 1
	elif plus.KeyPress(hg.KeyZ):
		charac_to_change -= 1

	if charac_to_change == -1:
		charac_to_change = 1
	elif charac_to_change == 3:
		charac_to_change = 0

	# REMOVE POINT
	if plus.KeyPress(hg.KeyQ):
		if charac_to_change == 0 and dexterity > 0:
			dexterity = min(max(dexterity - 1, 0), 10 - intelligence - rapidity)
			points += 1
			plus.GetMixer().Start(soundChoice)
		elif charac_to_change == 1 and intelligence > 0:
			intelligence = min(max(intelligence - 1, 0), 10 - dexterity - rapidity)
			points += 1
			plus.GetMixer().Start(soundChoice)
		elif charac_to_change == 2 and rapidity > 0:
			rapidity = min(max(rapidity - 1, 0), 10 - dexterity - intelligence)
			points += 1
			plus.GetMixer().Start(soundChoice)


	# ADD POINT
	if plus.KeyPress(hg.KeyD) and points > 0:
		if charac_to_change == 0:
			dexterity = min(max(dexterity + 1, 0), 10 - intelligence - rapidity)
		elif charac_to_change == 1:
			intelligence = min(max(intelligence + 1, 0), 10 - dexterity - rapidity)
		else:
			rapidity = min(max(rapidity + 1, 0), 10 - dexterity - intelligence)

		points -= 1
		plus.GetMixer().Start(soundChoice)

	# START GAME
	if plus.KeyPress(hg.KeySpace):
		channel_ambiante = plus.GetMixer().Start(soundAmbiance, hg.Mixer.RepeatState)
		StartGame()


def StartGame():
	global points, dexterity, intelligence, rapidity
	global laby1, scene, cam, fps, player
	global game_state, fade_value
	global channel_close_door, channel_open_door
	game_state = Game
	fade_value = 1
	if player != None:
		potion_nbr = player.getPotionNbr()
	else:
		potion_nbr = 10
	laby1, scene, cam, fps, player = InitGame(plus, dexterity, intelligence, rapidity)
	player.setPotionNbr(potion_nbr)
	dexterity = 0
	intelligence = 0
	rapidity = 0
	points = 10
	channel_close_door = None
	channel_open_door = None


def ReturnMenu():
	global game_state, fade_value
	game_state = Title
	EnregScore()
	ReloadGame()
	fade_value = 1


def Pause():
	global pause_option, game_state, fade_value, channel_ambiante

	plus.UpdateScene(scene, dt)

	channel_walking = GetChannelWalking()
	if channel_walking is not None:
		plus.GetMixer().Stop(channel_walking)

	color = hg.Color(0, 0, 0, 0.8)
	plus.Quad2D(0, 0, 0, screenHeight, screenWidth, screenHeight, screenWidth, 0, color, color,
				color, color, textureTitleBack)

	# OPTION COLOR
	if pause_option == 0:
		no_color = hg.Color(4/255, 124/255, 24/255)
		yes_color = hg.Color.White
	else:
		yes_color = hg.Color(4/255, 124/255, 24/255)
		no_color = hg.Color.White

	# DISPLAY
	plus.Text2D(screenWidth / 2 - 352, screenHeight / 2 + 250, "Do you want to return to the menu?", 50, hg.Color.White, text_font)
	plus.Text2D(screenWidth / 2 - 200, screenHeight / 2 + 150, " Yes", 35, yes_color, text_font)
	plus.Text2D(screenWidth / 2 + 150, screenHeight / 2 + 150, "No", 35, no_color, text_font)
	plus.Text2D(screenWidth / 2 - 295, screenHeight / 2 - 300, 'PRESS "SPACE" TO VALIDATE', 50, hg.Color.White,
				text_font)

	# NAVIGATION
	if plus.KeyPress(hg.KeyD):
		pause_option += 1
	elif plus.KeyPress(hg.KeyQ):
		pause_option -= 1

	if pause_option == -1:
		pause_option = 1
	elif pause_option == 2:
		pause_option = 0

	# CHANGE STATE
	if plus.KeyPress(hg.KeySpace):
		if pause_option == 0:
			game_state = Game
		else:
			pause_option = 0
			plus.GetMixer().Stop(channel_ambiante)
			ReturnMenu()


def Game():
	global message, game_state, cpt_win, fade_value, fade_coeff, channel_ambiante, channel_open_door, channel_close_door

	# DEBUG
	if fps_cam:
		fps.UpdateAndApplyToNode(cam, dt)

	# UPDATE
	plus.UpdateScene(scene, dt)
	message = UpdateMessage(message)

	# DISPLAY
	back_color = hg.Color(0, 0, 0, 0.7)
	plus.Quad2D(-20, screenHeight - 130, -20, screenHeight, screenWidth, screenHeight, screenWidth, screenHeight - 130, back_color, back_color, back_color, back_color)


	plus.Text2D(screenWidth - 300, screenHeight - 50, "TIME : " + str(GetTimer()), 40, hg.Color.White,
				text_font)
	plus.Text2D(screenWidth / 2 - 88, screenHeight - 50, "Labyrinth " + str(GetNLoop() + 1), 40, hg.Color.White,
				text_font)

	plus.Text2D(100, screenHeight - 70, str(player.getHealth()), 40, hg.Color.White, text_font)
	plus.Quad2D(10, screenHeight - 90, 10, screenHeight - 20, 90, screenHeight - 20, 90, screenHeight - 90,
				hg.Color.White, hg.Color.White,
				hg.Color.White, hg.Color.White, textureHeart)

	potion_nbr = player.getPotionNbr()
	plus.Quad2D(180, screenHeight - 110, 180, screenHeight - 10, 280, screenHeight - 10, 280, screenHeight - 110,
				hg.Color.White, hg.Color.White,
				hg.Color.White, hg.Color.White, texturePotion_2)
	plus.Text2D(280, screenHeight - 70, str(potion_nbr), 40, hg.Color.White, text_font)

	if player.hasKey():
		plus.Quad2D(370, screenHeight - 90, 370, screenHeight - 20, 460, screenHeight - 20, 460, screenHeight - 90, hg.Color.White, hg.Color.White,
					hg.Color.White, hg.Color.White, textureKey_2)

	if message.delay > 0:
		plus.Text2D(message.x, message.y, message.text, message.size, message.color, message.font)
		message.delay -= hg.time_to_sec_f(plus.GetClockDt())

	# PLAYER DIE
	if PlayerIsDie(player, message, plus):

		channel_walking = GetChannelWalking()
		if channel_walking is not None:
			plus.GetMixer().Stop(channel_walking)

		# UPDATE
		message = UpdateMessage(message)

		if message.delay <= 0:
			# FADE
			fade_value += fade_coeff
			color = hg.Color(0, 0, 0, fade_value)
			plus.Quad2D(0, 0, 0, screenHeight, screenWidth, screenHeight, screenWidth, 0, color, color, color, color)

			if fade_value >= 1:
				# CHANGE STATE
				plus.GetMixer().Stop(channel_ambiante)
				ReturnMenu()

	# PLAYER ALIVE
	else:

		# UPDATE
		UpdatePlayer(player, laby1.roads, plus)
		UpdateCamera(cam, player)

		# PLAYER WIN
		if PlayerHasWin(player, plus):
			message = UpdateMessage(message)
			if message.delay <= 0:

				#FADE
				if fade_value < 1:
					fade_value += fade_coeff
					color = hg.Color(0, 0, 0, fade_value)
					plus.Quad2D(0, 0, 0, screenHeight, screenWidth, screenHeight, screenWidth, 0, color, color, color,
								color)
					if channel_open_door is None:
						channel_open_door = plus.GetMixer().Start(soundOpenDoor)

				# CHANGE STATE
				else:
					color = hg.Color(0, 0, 0, 1)
					plus.Quad2D(0, 0, 0, screenHeight, screenWidth, screenHeight, screenWidth, 0, color, color, color,
								color)
					UpdateScore()
					UpdateGame()
					StartGame()

		else:
			# FADE
			if fade_value > 0:
				fade_value -= fade_coeff
				color = hg.Color(0, 0, 0, fade_value)
				plus.Quad2D(0, 0, 0, screenHeight, screenWidth, screenHeight, screenWidth, 0, color, color, color,
							color)

				if channel_close_door is None:
					channel_close_door = plus.GetMixer().Start(soundCloseDoor)

			UpdateTimer(hg.time_to_sec_f(plus.GetClockDt()))

	# PAUSE MENU
	if plus.KeyPress(hg.KeyP):
		game_state = Pause

	if plus.KeyPress(hg.KeyC):
		ChangeCameraView(cam)
		UpdateCamera(cam, player)



# laby1, scene, cam, fps = InitDebug(plus)
#
# def Debug():
# 	fps.UpdateAndApplyToNode(cam, dt)
# 	plus.UpdateScene(scene, dt)

game_state = Title
fade_value = 1
fade_coeff = 0.005
InitSound(plus)

# MAIN LOOP
dbg_visuals = False 

plus.SetBlend2D(hg.BlendAlpha)

while not plus.IsAppEnded():

	plus.Clear()
	dt = plus.UpdateClock()

	game_state()

	plus.Flip()
	plus.EndFrame()

	if plus.KeyPress(hg.KeyF10):
		fps_cam = not fps_cam

	if plus.KeyPress(hg.KeyF11):
		dbg_visuals = not dbg_visuals

	if plus.KeyPress(hg.KeyF2) and scene is not None:
		scene.Save("../work/Save_Scene/test1.scn", plus.GetRenderSystem())

	if dbg_visuals:
		import time
		time.sleep(1.5)


plus.SetBlend2D(hg.BlendOpaque)